defmodule TestBackend do
  use GenServer
  
  @vsn 2
  
  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: TestBackend)
  end
  
  def get_state do
    GenServer.call(TestBackend, :get_state)
  end
  
  def init(:ok) do
    {:ok, {:version2, :new_state, 0}}
  end
  
  def handle_call(:get_state, _from, {version, state, get_count}) do
    new_state = {version, state, get_count + 1}
    {:reply, new_state, new_state}
  end
  
  def code_change(_old_vsn, {_version, _state, get_count}, _extra) do
    {:ok, {:version2, :new_state, get_count}}
  end

end
