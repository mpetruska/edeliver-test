defmodule Web.ApiController do
  use Web.Web, :controller

  def get_state(conn, _params) do
    text conn, :io_lib.format("~w", [TestBackend.get_state])
  end
  
end
