#!/bin/bash

mkdir node/keys
cp ../keys/node_rsa.pub node/keys/node_rsa.pub
cp ../apps/web/config/prod.secret.exs node/keys/prod.secret.exs
docker build --tag=edeliver-test-node:0.1 node
