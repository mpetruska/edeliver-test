# EdeliverTest

## Pre-requisites

1. Erlang installed (e.g.: `brew install erlang`)
2. Elixir installed (e.g.: `brew install elixir`)

## Usage

### Create ssh key


    mkdir keys
    ssh-keygen

Path to provide to ssh-keygen: <...>/edeliver-test/keys

Where `<...>/edeliver-test` corresponds to the git project root.

Leave password empty.

    ssh-add <...>/edeliver-test/keys/node_rsa

### Add ssh config

Add the following line to the end of your `.ssh/config` file:

```
Host edeliver-test-build
  HostName 192.168.99.100
  Port 10022
  IdentityFile <...>/edeliver-test/keys/node_rsa
  StrictHostKeyChecking no
  UserKnownHostsFile /dev/null

Host edeliver-test-server
  HostName 192.168.99.100
  Port 10023
  IdentityFile <...>/edeliver-test/keys/node_rsa
  StrictHostKeyChecking no
  UserKnownHostsFile /dev/null
```

Where `192.168.99.100` is the ip address of the docker host (`docker-machine ip`).

Where `<...>/edeliver-test` corresponds to the git project root.

### Build node image

    cd docker
    ./build-node.sh

Note: this can take a very long time.

### Start nodes

    cd docker
    ./start_nodes.sh

### Build initial release

From project root driectory:

    MIX_ENV=prod mix edeliver build release --tag=0.1.0

Build the initial release `.deliver/releases/edeliver_test_0.1.0.release.tar.gz`

### Deploy initial release

From project root directory:

    mix edeliver deploy release to production --version=0.1.0

### Start the application

From project root directory:

    mix edeliver start production

### Open application home in browser

Open `192.168.99.100:10080` in a browser.

Where `192.168.99.100` is the ip address of the docker host (`docker-machine ip`).

### Build an upgrade

From project root driectory:

    MIX_ENV=prod mix edeliver build upgrade --from=0.1.0 --to=0.2.0

### Deploy the upgrade

From project root directory:

    mix edeliver deploy upgrade to production

### Additional commands

Stop application: `mix edeliver stop production`
